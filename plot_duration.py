
import sys

import pandas as pd
import networkx as nx
import matplotlib.pyplot as plt


# Load data
if len(sys.argv) > 1:
    filename = sys.argv[1]
else:
    filename = './common_connection_200k.csv'

df = pd.read_csv(filename)
col_1, col_2 = df.columns
G = nx.from_pandas_dataframe(df, source=col_1,
                             target=col_2)


degree_dict = G.degree()
# Sort nodes by degree in descending order
degree_list = sorted((node for node in degree_dict),
                     key=lambda x: degree_dict[x], reverse=True)

lines = []
with open('solution.log', 'r') as f:
    for line in f:
        if "Looped" in line:
            lines.append(line.split(',')[0])

times = pd.DataFrame(lines, columns=['time'])

t = pd.to_datetime(times.time, unit='ns')  # format='%Y-%m-%d %H:%M:%S')

intervals = [int(t.values[i + 1] - t.values[i]) / (1000000000 * 60) for i in range(len(t) - 1)]

plt.plot(list(range(len(intervals))), intervals)

plt.title('Duration of each loop')
plt.ylabel('Duration')
plt.xlabel('Iteration')

plt.savefig('loop_duration.pdf')


plt.plot([degree_dict[degree_list[i]] for i in range(len(intervals))],
         intervals, '.')

plt.title('Duration by Degree')
plt.xlabel("Degree")
plt.ylabel("Duration (minutes)")
plt.savefig("degree_duration.pdf")
