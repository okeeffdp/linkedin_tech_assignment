
"""
Author: Daniel O'Keeffe
Date:   2018-07-06
Title:  daniel_okeeffe_linkedIn_solution.py
"""
import sys
import logging

import networkx as nx
import pandas as pd
try:
    import queue
except ImportError:
    import Queue as queue


logging.basicConfig(format='%(asctime)s\t%(levelname)s\t%(message)s',
                    level=logging.INFO)


def most_common_neighbours(G):
    """
    Find the two nodes in G with the highest number of neighbours, but
    who are not neighbours themselves.

    To find the nodes with the most common neighbours, the algorithm first
    sorts the nodes by their degree. It takes the two nodes with the highest
    degrees, checks if the have an edge connecting them, if they do not,
    it will count the number of common neighbours and update the counter
    accordingly.

    If the two nodes do have an edge, it moves onto the next iteration.
    On the next iteration, it will retain the node with the highest degree,
    drop the node with the second highest degree, and compare the first node
    with the node with the third highest degree.

    The first node is compared to all other nodes in the network whose degree
    is higher than the current common neighbour count, `max_cn`.

    After comparing the first node to all others, the node with the second
    highest degree is compared to all nodes with fewer edges.

    This continues until there are fewer than two nodes for comparison.

    On each update to the `max_cn`, the nodes in the
    network with fewer connections than `max_cn` are excluded from the next
    iteration.

    Parameters
    ----------
    G : networkx.
        The network with which to analyse

    Returns
    -------
    max_cn_pair : 2-tuple of node ids
        The two nodes in G with the highest number of common neighbors
    """
    degree_dict = dict(G.degree())
    # Sort nodes by degree in descending order
    degree_list = sorted((node for node in degree_dict),
                         key=lambda x: degree_dict[x], reverse=True)

    curr_Q = queue.Queue()
    next_Q = queue.Queue()

    for node in degree_list:
        curr_Q.put(node)

    logging.info('Queue created.')
    # Pointers
    max_cn = 0
    max_cn_pair = (None, None)

    c = 1
    while curr_Q.qsize() >= 2:
        left, right = curr_Q.get(), curr_Q.get()

        while max_cn < degree_dict.get(right):

            if not G.has_edge(left, right):
                # From Mariah Walton
                paths = set(G.neighbors(left)).intersection(G.neighbors(right))
                path_num = len(list(paths))
                if max_cn < path_num:
                    max_cn = path_num
                    max_cn_pair = (left, right)
                    logging.info("New max found.")
                    logging.info(max_cn_pair)
                    logging.info(max_cn)

            next_Q.put(right)
            if curr_Q.empty():
                break
            right = curr_Q.get()

        curr_Q = next_Q
        next_Q = queue.Queue()

        logging.info('Looped {} times.'.format(c))
        logging.info(max_cn_pair)
        logging.info(max_cn)

        c += 1
    return(max_cn_pair)


if __name__ == '__main__':
    # Load data
    if len(sys.argv) > 1:
        filename = sys.argv[1]
    else:
        filename = './common_connection_200k.csv'

    df = pd.read_csv(filename)
    col_1, col_2 = df.columns
    G = nx.from_pandas_edgelist(df, source=col_1,
                                target=col_2)

    logging.info('Graph created.')

    max_cn_pair = most_common_neighbours(G)
    logging.info("Most common neighbours:\t{}, {}".format(*max_cn_pair))
